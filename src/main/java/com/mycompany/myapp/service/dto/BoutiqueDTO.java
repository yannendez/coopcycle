package com.mycompany.myapp.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link com.mycompany.myapp.domain.Boutique} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class BoutiqueDTO implements Serializable {

    private Long id;

    @NotNull
    private Integer idBoutique;

    private String nom;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdBoutique() {
        return idBoutique;
    }

    public void setIdBoutique(Integer idBoutique) {
        this.idBoutique = idBoutique;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BoutiqueDTO)) {
            return false;
        }

        BoutiqueDTO boutiqueDTO = (BoutiqueDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, boutiqueDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "BoutiqueDTO{" +
            "id=" + getId() +
            ", idBoutique=" + getIdBoutique() +
            ", nom='" + getNom() + "'" +
            "}";
    }
}
