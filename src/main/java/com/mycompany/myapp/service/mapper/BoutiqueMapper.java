package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Boutique;
import com.mycompany.myapp.service.dto.BoutiqueDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Boutique} and its DTO {@link BoutiqueDTO}.
 */
@Mapper(componentModel = "spring")
public interface BoutiqueMapper extends EntityMapper<BoutiqueDTO, Boutique> {}
