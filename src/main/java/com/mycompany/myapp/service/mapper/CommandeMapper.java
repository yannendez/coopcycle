package com.mycompany.myapp.service.mapper;

import com.mycompany.myapp.domain.Commande;
import com.mycompany.myapp.domain.Course;
import com.mycompany.myapp.domain.Paiement;
import com.mycompany.myapp.domain.Produit;
import com.mycompany.myapp.domain.Utilisateur;
import com.mycompany.myapp.service.dto.CommandeDTO;
import com.mycompany.myapp.service.dto.CourseDTO;
import com.mycompany.myapp.service.dto.PaiementDTO;
import com.mycompany.myapp.service.dto.ProduitDTO;
import com.mycompany.myapp.service.dto.UtilisateurDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commande} and its DTO {@link CommandeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {
    @Mapping(target = "paiement", source = "paiement", qualifiedByName = "paiementId")
    @Mapping(target = "course", source = "course", qualifiedByName = "courseId")
    @Mapping(target = "produits", source = "produits", qualifiedByName = "produitIdSet")
    @Mapping(target = "utilisateur", source = "utilisateur", qualifiedByName = "utilisateurId")
    CommandeDTO toDto(Commande s);

    @Mapping(target = "removeProduit", ignore = true)
    Commande toEntity(CommandeDTO commandeDTO);

    @Named("paiementId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    PaiementDTO toDtoPaiementId(Paiement paiement);

    @Named("courseId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CourseDTO toDtoCourseId(Course course);

    @Named("produitId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitDTO toDtoProduitId(Produit produit);

    @Named("produitIdSet")
    default Set<ProduitDTO> toDtoProduitIdSet(Set<Produit> produit) {
        return produit.stream().map(this::toDtoProduitId).collect(Collectors.toSet());
    }

    @Named("utilisateurId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    UtilisateurDTO toDtoUtilisateurId(Utilisateur utilisateur);
}
