package com.mycompany.myapp.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Paiement.
 */
@Entity
@Table(name = "paiement")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Paiement implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_paiement", nullable = false)
    private Integer idPaiement;

    @Column(name = "montant")
    private Integer montant;

    @JsonIgnoreProperties(value = { "paiement", "course", "produits", "utilisateur" }, allowSetters = true)
    @OneToOne(mappedBy = "paiement")
    private Commande commande;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Paiement id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getIdPaiement() {
        return this.idPaiement;
    }

    public Paiement idPaiement(Integer idPaiement) {
        this.setIdPaiement(idPaiement);
        return this;
    }

    public void setIdPaiement(Integer idPaiement) {
        this.idPaiement = idPaiement;
    }

    public Integer getMontant() {
        return this.montant;
    }

    public Paiement montant(Integer montant) {
        this.setMontant(montant);
        return this;
    }

    public void setMontant(Integer montant) {
        this.montant = montant;
    }

    public Commande getCommande() {
        return this.commande;
    }

    public void setCommande(Commande commande) {
        if (this.commande != null) {
            this.commande.setPaiement(null);
        }
        if (commande != null) {
            commande.setPaiement(this);
        }
        this.commande = commande;
    }

    public Paiement commande(Commande commande) {
        this.setCommande(commande);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Paiement)) {
            return false;
        }
        return id != null && id.equals(((Paiement) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Paiement{" +
            "id=" + getId() +
            ", idPaiement=" + getIdPaiement() +
            ", montant=" + getMontant() +
            "}";
    }
}
