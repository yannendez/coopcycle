import { ICommande } from 'app/shared/model/commande.model';
import { ICourse } from 'app/shared/model/course.model';

export interface IUtilisateur {
  id?: number;
  idUtilisateur?: number;
  nom?: string | null;
  prenom?: string | null;
  commandes?: ICommande[] | null;
  courses?: ICourse[] | null;
}

export const defaultValue: Readonly<IUtilisateur> = {};
