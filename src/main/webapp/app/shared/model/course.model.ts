import { ICommande } from 'app/shared/model/commande.model';
import { IUtilisateur } from 'app/shared/model/utilisateur.model';

export interface ICourse {
  id?: number;
  idCourse?: number;
  commande?: ICommande | null;
  utilisateur?: IUtilisateur | null;
}

export const defaultValue: Readonly<ICourse> = {};
