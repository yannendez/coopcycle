import { ICommande } from 'app/shared/model/commande.model';

export interface IPaiement {
  id?: number;
  idPaiement?: number;
  montant?: number | null;
  commande?: ICommande | null;
}

export const defaultValue: Readonly<IPaiement> = {};
