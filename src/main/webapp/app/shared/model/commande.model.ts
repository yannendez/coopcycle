import { IPaiement } from 'app/shared/model/paiement.model';
import { ICourse } from 'app/shared/model/course.model';
import { IProduit } from 'app/shared/model/produit.model';
import { IUtilisateur } from 'app/shared/model/utilisateur.model';

export interface ICommande {
  id?: number;
  idCommande?: number;
  paiement?: IPaiement | null;
  course?: ICourse | null;
  produits?: IProduit[] | null;
  utilisateur?: IUtilisateur | null;
}

export const defaultValue: Readonly<ICommande> = {};
