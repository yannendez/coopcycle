import React from 'react';
import { Route } from 'react-router-dom';

import ErrorBoundaryRoutes from 'app/shared/error/error-boundary-routes';

import Boutique from './boutique';
import Produit from './produit';
import Commande from './commande';
import Paiement from './paiement';
import Utilisateur from './utilisateur';
import Course from './course';
/* jhipster-needle-add-route-import - JHipster will add routes here */

export default () => {
  return (
    <div>
      <ErrorBoundaryRoutes>
        {/* prettier-ignore */}
        <Route path="boutique/*" element={<Boutique />} />
        <Route path="produit/*" element={<Produit />} />
        <Route path="commande/*" element={<Commande />} />
        <Route path="paiement/*" element={<Paiement />} />
        <Route path="utilisateur/*" element={<Utilisateur />} />
        <Route path="course/*" element={<Course />} />
        {/* jhipster-needle-add-route-path - JHipster will add routes here */}
      </ErrorBoundaryRoutes>
    </div>
  );
};
