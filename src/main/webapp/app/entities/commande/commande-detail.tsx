import React, { useEffect } from 'react';
import { Link, useParams } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { Translate } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';
import { useAppDispatch, useAppSelector } from 'app/config/store';

import { getEntity } from './commande.reducer';

export const CommandeDetail = () => {
  const dispatch = useAppDispatch();

  const { id } = useParams<'id'>();

  useEffect(() => {
    dispatch(getEntity(id));
  }, []);

  const commandeEntity = useAppSelector(state => state.commande.entity);
  return (
    <Row>
      <Col md="8">
        <h2 data-cy="commandeDetailsHeading">
          <Translate contentKey="cooopcycleApp.commande.detail.title">Commande</Translate>
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="id">
              <Translate contentKey="global.field.id">ID</Translate>
            </span>
          </dt>
          <dd>{commandeEntity.id}</dd>
          <dt>
            <span id="idCommande">
              <Translate contentKey="cooopcycleApp.commande.idCommande">Id Commande</Translate>
            </span>
          </dt>
          <dd>{commandeEntity.idCommande}</dd>
          <dt>
            <Translate contentKey="cooopcycleApp.commande.paiement">Paiement</Translate>
          </dt>
          <dd>{commandeEntity.paiement ? commandeEntity.paiement.id : ''}</dd>
          <dt>
            <Translate contentKey="cooopcycleApp.commande.course">Course</Translate>
          </dt>
          <dd>{commandeEntity.course ? commandeEntity.course.id : ''}</dd>
          <dt>
            <Translate contentKey="cooopcycleApp.commande.produit">Produit</Translate>
          </dt>
          <dd>
            {commandeEntity.produits
              ? commandeEntity.produits.map((val, i) => (
                  <span key={val.id}>
                    <a>{val.id}</a>
                    {commandeEntity.produits && i === commandeEntity.produits.length - 1 ? '' : ', '}
                  </span>
                ))
              : null}
          </dd>
          <dt>
            <Translate contentKey="cooopcycleApp.commande.utilisateur">Utilisateur</Translate>
          </dt>
          <dd>{commandeEntity.utilisateur ? commandeEntity.utilisateur.id : ''}</dd>
        </dl>
        <Button tag={Link} to="/commande" replace color="info" data-cy="entityDetailsBackButton">
          <FontAwesomeIcon icon="arrow-left" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.back">Back</Translate>
          </span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/commande/${commandeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" />{' '}
          <span className="d-none d-md-inline">
            <Translate contentKey="entity.action.edit">Edit</Translate>
          </span>
        </Button>
      </Col>
    </Row>
  );
};

export default CommandeDetail;
