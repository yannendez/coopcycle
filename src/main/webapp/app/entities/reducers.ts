import boutique from 'app/entities/boutique/boutique.reducer';
import produit from 'app/entities/produit/produit.reducer';
import commande from 'app/entities/commande/commande.reducer';
import paiement from 'app/entities/paiement/paiement.reducer';
import utilisateur from 'app/entities/utilisateur/utilisateur.reducer';
import course from 'app/entities/course/course.reducer';
/* jhipster-needle-add-reducer-import - JHipster will add reducer here */

const entitiesReducers = {
  boutique,
  produit,
  commande,
  paiement,
  utilisateur,
  course,
  /* jhipster-needle-add-reducer-combine - JHipster will add reducer here */
};

export default entitiesReducers;
