# Rapport du DM No Code de Yann Endezoumou

## Page 1

- lien vers le repo gitlab =  https://gitlab.com/yannendez/coopcycle.git
- lien dans le gitlab vers le ficher coopcycle.jh : https://gitlab.com/yannendez/coopcycle/-/blob/master/coopcycle.jh
- photo de la représentation graphique : ![image du modèle de coopcycle](coopcycle.png)
- lien de la représentation graphique dans le gitlab : https://gitlab.com/yannendez/coopcycle/-/blob/master/coopcycle.png

## Page 2
- liste de commandes pour lancer jhipster :
```
- mkdir coopcycle
- cd coopcycle
- jhipster
(entrée de toutes les configurations du projet)
- npm start (pour lancer le serveur)
- ./mvnw (pour lancer l'application)
```

- photo des réponses pour la configuration de l'application via jhipster : ![texte alt](jhipster_maven.png)

- copie d'écran d'une des entités choisie pour l'application coopcycle : 
  ![](image-entite.png)

## Page 3

- nombre de lignes de codes générées par jhipster : 20442 lignes
  ![](ligne_code_jhipster.png)
- nombre de ligne de code générée par l'application + le modèle : 32015 lignes
  ![](ligne_code+model.png)

- coût mensuel d'un full-stack developpeur :
  - France : 3333€ 
  - Inde : 500€
  - USA : 8699$

__calcul cocomo__ : 

valeure pour le champs new : 10% du code généré  = 0.1 * 32015 = 3202

valeur pour le champs used (pas mentionné dans le sujet donc 0)

valeur pour le champs modified = 0.2 * 32015 = 6403

**resultat**
  - France : effort = 10.6, temps = 7-8 mois, cout = 37972
  - USA : effort = 10.6, temps = 7-8 mois, cout = 99131$
  - Inde : effort = 10.6, temps = 7-8 mois, cout = 5696$

## Page 4

![](api_swagger.png)

## Page 5

Pour cette partie une erreur est survenu :
![](erreur-sonar.png)

## Page 6
Erreur lors du déploiement de l'application
![](erreur_depl.png)
